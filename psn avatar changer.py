#PSN Avatar Changer
#SilicaAndPina 11/08/2018

import requests

token = "undefined" #This is specific to your PSN account.
url = "https://us-prof.np.community.playstation.net/userProfile/v1/users/me/avatar"

if token == "undefined":
	print("Goto: https://id.sonyentertainmentnetwork.com/id/management/#/p/psn_profile/list?entry=psn_profile in chrome")
	print("Open developer tools and goto the 'Network' tab")
	print("login to the site (or reload the page if already logged in)")
	print("Search all headers for 'Bearer', The one thats used the most is your 'Bearer' Token")
	print("See: https://youtu.be/h25kMCztyUg for basic usage")
	print("")
	token = raw_input("Enter PSN 'Bearer' Token: ")

headers = { "Content-Type":"application/json; charset=UTF-8",
			"Origin":"https://id.sonyentertainmentnetwork.com",
			"Referer":"https://id.sonyentertainmentnetwork.com/id/management/",
			"User-Agent":"Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/68.0.3440.106 Safari/537.36",
			"Authorization":"Bearer "+token}
while True:

	print("-- PSN Avatar Changer P.O.C By SilicaAndPina --")
	print("")
	print("1. Change to Non-Premium Avatar")
	print("2. Change to Premium Avatar (You must own the avatar)")
	print("3. JUST GIMMIE THE DEFAULT HAPPY FACE AVATAR BACK PLZZ!")
	print("4. Exit program.")

	while True:
		try: 
			while True:
				selection = int(raw_input("Make Choice 1-4: "))
				if selection < 1:
					print("Invalid Input")
					continue
				if selection > 4:
					print("Invalid Input")
					continue
				break
		except:
			continue
		break

	if selection == 1:
		avatarID = raw_input("AvatarID: ")
		print("Attempting to contact PSN Server ...")
		requestHandle = requests.put(url=url,headers=headers,data='{"avatarId":'+str(avatarID)+'}')
		print("Got Code: "+str(requestHandle.status_code))
		if requestHandle.status_code == 204:
			print("Avatar changed successfully!")
		else:
			print(requestHandle.text)
		
	if selection == 2:
		EntitlementID = raw_input("EntitlementID (aka Content_ID): ")
		print("Attempting to contact PSN Server ...")
		requestHandle = requests.put(url=url,headers=headers,data='{"entitlementId":"'+str(EntitlementID)+'"}')
		print("Got Code: "+str(requestHandle.status_code))
		if requestHandle.status_code == 204:
			print("Avatar changed successfully!")
		else:
			print(requestHandle.text)

	if selection == 3:
		print("I understand your pain... everyone deserves a happy face avatar~")
		print("/-----------\\")
		print("| (-)   (-)  |")
		print("|            |")
		print("|  -------   |")
		print("|            |")
		print("\\------------/")
		print("Attempting to contact PSN Server ...")
		requestHandle = requests.put(url=url,headers=headers,data='{"avatarId":0}')
		print("Got Code: "+str(requestHandle.status_code))
		if requestHandle.status_code == 204:
			print("Avatar changed successfully! :) enjoy the happy face")
		else:
			print(requestHandle.text)

	if selection == 4:
		print("Goodbye!")
		sys.exit()
		break